#!/usr/bin/env python
# coding: utf-8

# Import libs


from utils.neural_tangent_denoiser import NeuralTangentDenoiser
from utils.common_utils import *
import json
import argparse
import os
import scipy.misc

def save_results(self):
    import json
    dict = {'mse': self.error, 'psnr': self.psnr_iter, 'best_psnr': self.best_psnr}

    json = json.dumps(dict)
    f = open("dict.json", "w")
    f.write(json)
    f.close()

def get_freer_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
    print('GPU id ' + str(np.argmax(memory_available)) + '|free memory: ' + str(np.max(memory_available)))
    return np.argmax(memory_available)

parser = argparse.ArgumentParser(description='Denoising parameters.')
parser.add_argument('--channels', type=int, default=128, help='number of hidden layer channels')
parser.add_argument('--kernel-size',  type=int, default=3, help='size of the convolution kernel')
parser.add_argument('--show-every',  type=int, default=1000, help='size of the convolution kernel')
parser.add_argument('--depth',  type=int, default=1, help='size of the convolution kernel')
parser.add_argument('--sigma',  type=int, default=25, help='standard deviation of the noise \sigma \in [0, 100]')
parser.add_argument('--imsize',  type=int, default=-1, help='size of the image: -1 for original '
                                                           'image size, otherwise size in pixels e.g. 64')
parser.add_argument('--no-plot',  action='store_true', help='do not plot denoising progress')
parser.add_argument('--gray',  action='store_true', help='process grayscale image')
parser.add_argument('--remove-init',  action='store_true', help='remove initialization from the CNNs output')
parser.add_argument('--input-image',  action='store_true', help='place noisy image at the input'
                                                                ', otherwise place just noise')
parser.add_argument('--vanilla',  action='store_true', help='use vanilla CNN, otherwise uses UNET')
parser.add_argument('--noise2self', action='store_true', help='use noise2self (N2S) training'
                                                               ', please see the N2S paper for details')
parser.add_argument('--gd', action='store_true', help=' use gradient descent, otherwise use Adam')
parser.add_argument('-dir', '--save-dir', default='results', help='directory to save images')
parser.add_argument('--cpu', action='store_true', help=' use CPU instead of cuda')
parser.add_argument('--nystrom', action='store_true', help=' used closed form kernel with nystrom approximation CPU instead of cuda')
parser.add_argument('--show-ntk',  action='store_true', help='compute and show the NTK at initialization'
                                                             ' and after training (WARNING: this might be'
                                                             ' slow for large images)')
parser.add_argument('--show-preactivations',  action='store_true', help='show the preactivations during'
                                                                        ' training (WARNING: this might be'
                                                                        ' slow for large images)')


params = parser.parse_args()

params.nystrom = True
params.input_image = True
params.remove_init = True
params.kernel_size = 11
params.gd = True
params.vanilla = True
params.save_dir = 'results/nystrom/eigen'

# Load image
dataset_filenames = ['baboon']
avg_psnr = 0

free_gpu_id = get_freer_gpu()
torch.cuda.set_device(int(free_gpu_id))

for filename in dataset_filenames:
    print('Processing '+ str(filename))
    img_np, img_noisy_np = get_image('data/color_dataset/' + filename + '.png', params.imsize, params.gray, params.sigma, d=32)
    ntd = NeuralTangentDenoiser(img_noisy_np, img_np, params)
    img_denoised, stats = ntd.denoise()

    for i in range(6):
        img_eig = ntd.U[:, i].view(1, img_np.shape[1], img_np.shape[2]).detach().cpu().numpy()
        im = np_to_pil(img_eig)
        im.save(params.save_dir + '/eigen' + str(i) + '_'+ filename + '.png')



