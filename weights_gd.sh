#!/usr/bin/env bash

for chan in 8 16 32 64 128 256 512
do
    python3 denoising_dataset.py --channels $chan --gd --mc-iter 10 --no-plot --single-image -dir results/gd/unet/noise/ch$chan
done

