#!/usr/bin/env bash

python3 denoising_dataset.py --gd --remove-init --input-image --no-plot -dir results/gd/unet/image

python3 denoising_dataset.py --gd --remove-init  --no-plot -dir results/gd/unet/noise

python3 denoising_dataset.py --gd --vanilla --channels 512 --kernel-size 11  --remove-init --input-image --no-plot -dir results/gd/vanilla/image

python3 denoising_dataset.py --gd --vanilla --channels 512 --kernel-size 11 --remove-init --no-plot -dir results/gd/vanilla/noise