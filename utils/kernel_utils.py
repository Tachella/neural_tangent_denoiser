import torch
import torch.nn.functional as F
import numpy as np
pi = 3.1415927410125732

def extract_image_patches(x, kernel, stride=1):
    kernel = int(kernel)
    # from (1,C,H,W) to (C,H*W,kernel,kernel)
    to_pad = int((kernel - 1) / 2)
    patches = F.pad(x, [to_pad, to_pad, to_pad, to_pad], 'reflect') #
    patches = patches.unfold(2, kernel, stride).unfold(3, kernel, stride) # (1, C, H, W, kernel, kernel)
    patches = patches.permute(0, 4, 5, 1, 2, 3) # (1, kernel,kernel, C, H, W)
    patches = patches.contiguous()
    patches = patches.view(*patches.size()[:4], -1) # (1, kernel, kernel, C, H*W)
    patches = patches.transpose(0, 4) # (H*W, kernel, kernel, C)
    patches = patches.unsqueeze(1).transpose(1, 4) # (H*W, C, kernel, kernel, 1)
    patches = patches.squeeze().unsqueeze(0) # (1, H*W, C, kernel, kernel)
    # patches /= patches.size()[1]
    # print(patches.size())
    return patches


def global_filtering(x, points, config, thres=0.001, method='nystrom'):
    psize = (config['kernel_size']-1)/2
    r_field = int(2*psize*config['layers']+1)
    print('Receptive field: ' + str(r_field) + 'x'+ str(r_field))
    U, d = approximate_gram(extract_image_patches(x, r_field), points, config, method)

    # crop eigenvalues
    ind = d> torch.max(d)*thres
    U = U[:, ind]
    d = d[ind]
    print('Rank Gram matrix: ' + str(d.size()[0]))
    return U, d



def compute_kernel(patch, data, config):
    # k(patch, patch)
    psize = int(config['kernel_size'])
    patch = patch.permute(1, 0, 2, 3, 4) # (points, 1, C, kernel, kernel)
    xcorr = data*patch # (points, pixels, C, kernel, kernel) TODO: merge these steps to save memory
    xcorr = xcorr.mean(2) # (points, pixels, kernel, kernel)

    norms = torch.sqrt(F.avg_pool2d((data*data).mean(2), psize, stride=1))
    norm_patch = torch.sqrt(F.avg_pool2d((patch*patch).mean(2), psize, stride=1))

    eps = 1e-10
    # first layer
    xcorr = F.avg_pool2d(xcorr, psize, stride=1)
    s = norms * norm_patch

    prephi = (xcorr/(s+eps)).clamp(min=-1, max=1)
    phi = torch.acos(prephi)
    #print('Overflow layer 1: ' + str(torch.sum(torch.isnan(phi))))
    xcorr = s*(torch.sin(phi) + (pi*torch.ones_like(phi) - phi)*torch.cos(phi)) / pi
    ntk = xcorr

    # following layers
    for l in range(1, config['layers']):
        ntk -= ntk*(phi/pi)
        xcorr = F.avg_pool2d(xcorr, psize, stride=1)
        norms = torch.sqrt(F.avg_pool2d(norms*norms, psize, stride=1))
        norm_patch = torch.sqrt(F.avg_pool2d(norm_patch*norm_patch, psize, stride=1))
        s = norms * norm_patch

        prephi = (xcorr / (s + eps)).clamp(min=-1, max=1)
        phi = torch.acos(prephi)
        # print('Overflow layer' + str(l) + ': ' + str(torch.sum(xcorr > s)))
        xcorr = s * (torch.sin(phi) + (pi*torch.ones_like(phi) - phi) * torch.cos(phi)) / pi
        ntk = F.avg_pool2d(ntk, psize, stride=1)
        ntk += xcorr

    return ntk.squeeze()

def nystrom(K, pixels):
    _, d, U = torch.svd(K[:, pixels], some=True)

    U = torch.mm(K.t(), U)
    scaling = U.size()[1]/U.size()[0]
    U *= np.sqrt(scaling)
    for i in range(U.size()[1]):
        U[:, i] /= d[i]
    d /= scaling

    for i in range(U.size()[1]):
        scaling = U[:, i].norm()
        U[:, i] /= scaling
        d[i] *= (scaling**2)

    return U, d

def approximate_gram(x, points, config, method):
    if points > x.size()[1]:
        points = x.size()[1]

    nys_pixels = torch.from_numpy(np.random.choice(x.size()[1], points, replace=False))

    pixels = torch.arange(x.size()[1])
    batches = int(np.ceil(x.size()[1]*points/(128*128)))
    print('Processing data in ' + str(batches) + ' batches')
    pixel_batch = pixels.split(int(np.ceil(x.size()[1]/batches)))

    # process Nystrom pixels

    K = torch.zeros((points, x.size()[1]), device=config['device'])
    k = 0
    for pix in pixel_batch:
        K[:, k:k+pix.size()[0]] = compute_kernel(x[:, nys_pixels, :, :], x[:, k:k+pix.size()[0], :, :], config)
        k += pix.size()[0]


    del x, pixels, pixel_batch
    if config['device'] == 'cuda':
        torch.cuda.empty_cache()
    if method is 'nystrom':
        U, d = nystrom(K, nys_pixels)
    else:
        _, d, U = torch.svd(K, some=True)  # column subsampling approximation

    return U, d



