#!/usr/bin/env python
# coding: utf-8


# Import libs
from skimage.metrics import peak_signal_noise_ratio
import numpy as np
from models.simple_CNN import VanillaCNN, ResNet
from utils.kernel_utils import global_filtering
from matplotlib import pyplot as plt
# os.environ['CUDA_VISIBLE_DEVICES'] = '3'
from utils.common_utils import *
import torch


def forward_hook(self, input, output):
    print('Inside ' + self.__class__.__name__ + ' forward')
    if type(output) == tuple:
        output = output[0]

    s = output.size()
    J = output.view(s[1], s[2] * s[3])
    _, eigv, eigf = torch.svd(J)
    eigf = eigf[:, :5].permute(1, 0)
    out_np = torch_to_np(eigf.view(1, 5, s[2], s[3]))
    mosaic_imshow(out_np, 1, 5, 'iteration ' + str(self.i), out_np[0, :, :])


class NeuralTangentDenoiser:
    def __init__(self, img_noisy, img_oracle, params):
        super(NeuralTangentDenoiser, self).__init__()

        self.params = params
        if params.cpu:
            dtype = torch.FloatTensor
            self.device = 'cpu'
        else:
            torch.backends.cudnn.enabled = True
            torch.backends.cudnn.benchmark = True
            dtype = torch.cuda.FloatTensor
            self.device = 'cuda'

        self.max_iter = params.max_iter
        self.img_oracle = img_oracle
        self.img_noisy_torch = np_to_torch(img_noisy).type(dtype)
        self.best_out = torch.zeros_like(self.img_noisy_torch)
        self.imdims = self.img_noisy_torch.size()
        self.best_psnr = -999999.9
        self.best_iter = 0
        # Loss
        self.loss = torch.nn.MSELoss().type(dtype)
        self.img_noisy_torch = self.normalize_image(self.img_noisy_torch)

        if params.input_image:
            self.net_input = self.img_noisy_torch.clone()
        else:
            self.net_input = torch.randn_like(self.img_noisy_torch)

        if params.nystrom:
            config = {'layers': params.depth, 'kernel_size': params.kernel_size, 'device': self.device} #3 # 1, 3, 5 etc.
            nystrom_points = min(1200, int(self.imdims[2]*self.imdims[3]/50.))
            thres = 0.0001
            method = 'nystrom'
            self.U, self.d = global_filtering(self.net_input, nystrom_points, config, thres, method)
            self.d /= torch.max(self.d)
        else:
            if params.vanilla:
                self.net = VanillaCNN(self.imdims[1], params.depth, params.channels,  params.kernel_size).type(dtype)
                LR = 1./(4*(params.depth+1)*params.channels)
            else:
                self.net = ResNet(self.imdims[1], params.channels, params.kernel_size).type(dtype)
                if params.gd:
                    if params.noise2self:
                        LR = 200./(params.channels)
                    else:
                        LR = 1./(params.channels)
                else:
                    LR = 1./(20*params.channels)
            # Compute number of parameters
            s = sum([np.prod(list(p.size())) for p in self.net.parameters()])
            print('Number of params: %d' % s)
            # Load initial parameters
            p = []
            p += [x for x in self.net.parameters()]
            if params.gd:
                self.optimizer = torch.optim.SGD(p, lr=LR)
            else:
                self.optimizer = torch.optim.Adam(p, lr=LR, betas=(0.9, 0.99))

        if params.noise2self:
            d = torch.rand_like(self.img_noisy_torch)
            self.part_number = 25
            self.partitions = torch.zeros(self.part_number, self.imdims[1], self.imdims[2], self.imdims[3], device=self.device, dtype=torch.bool)
            self.random_labels = torch.rand_like(self.img_noisy_torch) - 0.5
            for r in range(self.part_number):
                self.partitions[r, :, :, :] = (d > (float(r)/self.part_number)) * (d < ((r+1.)/self.part_number))

        if self.params.show_preactivations and not params.gd:
            print('Showing pre-activations')
            self.net.convu3.register_forward_hook(self.forward_hook)
            # self.net.convd5.register_backward_hook(self.forward_hook)

        self._print_params()


    def _print_params(self):
        """Formats parameters to print when training."""
        print('Denoising parameters: ')
        param_dict = vars(self.params)
        pretty = lambda x: x.replace('_', ' ').capitalize()
        print('\n'.join('  {} = {}'.format(pretty(k), str(v)) for k, v in param_dict.items()))
        print()


    def change_to_gd(self):
        p = []
        p += [x for x in self.net.parameters()]
        LR = 1. / (self.params.channels)
        self.optimizer = torch.optim.SGD(p, lr=LR)
        self.max_iter = 1e6


    def denoise(self):
        if self.params.nystrom:
            out = torch.zeros_like(self.img_noisy_torch)
        else:
            self.first_param = []
            for f in self.net.parameters():
                self.first_param += [f.clone()]
            with torch.no_grad():
               self.init = self.net(self.net_input)

        self.error = []
        self.psnr_iter = []
        self.no_improvement = False
        self.no_imp_counter = 0
        self.best_i = 0
        self.best_psnr = -999999.9

        print('Starting twicing-based denoising')
        self.i = 0
        while self.i < self.max_iter and not self.no_improvement:
            if self.params.nystrom:
                out, mse = self.closure_nystrom(out)
            else:
                self.optimizer.zero_grad()
                out, mse = self.closure()
                self.optimizer.step()

            self.new_estimate_psnr(out)

            self.error.append(mse.item())
            self.i += 1
            print('\r Denoising performance [dB] ' + str(self.best_psnr) + ', best iter: ' + str(self.best_i)
                  + ', current iter ' + str(self.i), end="")
        print('Finished denoising: epochs ' + str(self.i))

        self.l2_change = []
        self.linf_change = []
        if not self.params.nystrom:
            for f, f0 in zip(self.net.parameters(), self.first_param):
                self.l2_change.append(np.sqrt(torch.sum(torch.pow(f.data.flatten() - f0.data.flatten(), 2)).item()))
                self.linf_change.append(torch.max(torch.pow(f.data.flatten() - f0.data.flatten(), 2)).item())

        if not self.params.no_plot:
            self.plot_stats()

        stats = {'mse': self.error, 'epochs': self.best_i, 'weight_l2_change': self.l2_change, 'weight_linf_change': self.linf_change,
                   'psnr': self.psnr_iter, 'best_psnr': self.best_psnr, 'params': vars(self.params)}

        return self.best_out, stats

    def closure_nystrom(self, out):
        res = (self.img_noisy_torch-out)
        for r in range(self.imdims[1]):
            out[0, r, :, :] = out[0, r, :, :] + (torch.mv(self.U, self.d*torch.mv(self.U.t(), res[0, r, :, :].flatten())).view(self.imdims[2], self.imdims[3]))

        total_loss = self.loss(out, self.img_noisy_torch)
        return out, total_loss

    def closure(self):

        if self.params.noise2self:
            p = self.i % self.part_number
            pixels = self.partitions[p, :, :, :].view(self.net_input.size())
            mask = torch.zeros_like(self.net_input)
            mask[pixels] = 1
            out = self.net(self.net_input*(torch.ones_like(mask)-mask) + mask*self.random_labels)
            total_loss = self.loss(out*mask, self.img_noisy_torch*mask)
        else:
            out = self.net(self.net_input)
            if self.params.remove_init:
                out -= self.init
            total_loss = self.loss(out, self.img_noisy_torch)

        total_loss.backward()

        if self.params.noise2self:
            out = self.net(self.net_input)

        return out, total_loss

    def new_estimate_psnr(self, out):
        outn = out.clone()
        outn = self.denormalize_image(outn)

        out_np = torch_to_np(outn).clip(0, 1)

        if self.i % self.params.show_every == 0 and not self.params.no_plot:
            plot_image_grid([out_np], 4, 5)

        psnr = peak_signal_noise_ratio(self.img_oracle, out_np)
        self.psnr_iter.append(psnr)

        if self.best_psnr < psnr:
            self.best_i = self.i
            self.best_psnr = psnr
            self.best_out = out_np
            self.no_imp_counter = 0
        else:
            self.no_imp_counter += 1

        if self.no_imp_counter > 1000:
            self.no_improvement = True

    def plot_stats(self):
        if self.params.nystrom:
            eig_np = torch_to_np(self.U[:self.imdims[2] * self.imdims[3], :6].t().view(1, 6, self.imdims[2], self.imdims[3]))
            mosaic_imshow(eig_np, 2, 3, 'Main EigenImages')

        plt.figure()
        plt.plot(np.log(self.error))
        plt.title('MSE')
        plt.xlabel('iteration')
        plt.show()

        plt.figure()
        plt.plot(self.psnr_iter)
        plt.ylabel('PSNR')
        plt.xlabel('iteration')
        plt.show()

    def normalize_image(self, img):
        with torch.no_grad():
            for r in range(img.size()[1]):
                img[0, r, :, :] -= .5
        return img

    def denormalize_image(self, img):
        with torch.no_grad():
            for r in range(img.size()[1]):
                img[0, r, :, :] += .5
        return img
