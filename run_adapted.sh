#!/usr/bin/env bash

python3 denoising_dataset.py --remove-init --input-image --no-plot --max-iter 5000 --change-to-gd -dir results/adapted_gd/unet/image