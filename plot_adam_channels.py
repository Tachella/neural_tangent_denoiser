import json
import os
import matplotlib.pyplot as plt
import numpy as np
from utils.common_utils import get_image, plot_image_grid
from matplotlib import rc
import matplotlib
from matplotlib.ticker import ScalarFormatter
rc('font', **{'size': 19})
#rc('text', usetex=True)

filename = 'house'
channels = [8, 16, 32, 64, 128, 256, 512]
mc_iter = 10
optimizers = ['adam', 'gd']
name_optimizers = ['Adam', 'Gradient descent']

fig1, ax1 = plt.subplots(num=1)
fig3, ax3 = plt.subplots(num=20)
fig2, ax2 = plt.subplots(num=10)

for opt, n_opt in zip(optimizers,name_optimizers):
    psnr = np.zeros((len(channels), mc_iter))
    w_change = np.zeros((len(channels), mc_iter))
    w_inf_change = np.zeros((len(channels), mc_iter))
    k = 0
    for ch in channels:
        for j in range(mc_iter):
            if j == 0:
                fname = filename
            else:
                fname = filename + str(j)
            dir = 'results/' + opt + '/unet/noise/ch' + str(ch) + '/stats_' + fname + '.json'
            if os.path.exists(dir):
                f = open(dir, "r")
                stats = json.load(f)

                t_change = 0
                for layer in stats['weight_l2_change'][2:-2]:
                    t_change += layer
                w_change[k, j] = t_change/len(stats['weight_l2_change'])

                t_change = 0
                for layer in stats['weight_linf_change'][2:-2]:
                    t_change += layer
                w_inf_change[k, j] = np.sqrt(t_change)/len(stats['weight_linf_change'])

                psnr[k, j] = stats['best_psnr']
                f.close()
        k += 1

    if opt == 'gd': ## TODO: remove this fix
        psnr[-1, 3:] = psnr[-1, 0]
        w_inf_change[-1, 3:] = w_inf_change[-1, 0]
        w_change[-1, 3:] = w_change[-1, 0]

    ## PSNR
    error = np.vstack((np.min(psnr, 1), np.max(psnr, 1)))
    error = np.abs(error - np.mean(psnr, 1))
    ax1.errorbar(channels, np.mean(psnr, 1), yerr=error, fmt='-o', label=n_opt)

    ## Weight change
    error = np.vstack((np.min(w_change, 1), np.max(w_change, 1)))
    error = np.abs(error - np.mean(w_change, 1))
    ax2.errorbar(channels, np.mean(w_change, 1), yerr=error, fmt='-o', label=n_opt)

    ## Weight change
    error = np.vstack((np.min(w_inf_change, 1), np.max(w_inf_change, 1)))
    error = np.abs(error - np.mean(w_inf_change, 1))
    ax3.errorbar(channels, np.mean(w_inf_change, 1), yerr=error, fmt='-o', label=n_opt)

ax1.set_xscale('log')
ax1.set_xlabel('channels')
ax1.set_ylabel('PSNR [dB]')
ax1.set_xticks(channels)
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
#ax1.legend()
fig1.savefig("figures/house_psnr.svg")

ax2.plot(channels, 1/np.power(np.array(channels), .5))
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlabel('channels')
ax2.set_ylabel('$||w^{t}-w^{0}||_2$')
ax2.set_xticks(channels)
ax2.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
#ax2.legend()
fig2.savefig("figures/l2_change.svg")

ax3.set_xscale('log')
ax3.set_yscale('log')
ax3.set_xlabel('channels')
ax3.set_ylabel('$||w^{t}-w^{0}||_\infty$')
ax3.set_xticks(channels)
ax3.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax3.legend()
fig3.savefig("figures/linf_change.svg")
plt.show()
