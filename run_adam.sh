#!/usr/bin/env bash

# adam|unet|noise input
python3 denoising_dataset.py --no-plot -dir results/adam/unet/noise

# adam|unet|image input
python3 denoising_dataset.py --remove-init --input-image --no-plot -dir results/adam/unet/image

# adam|vanilla|noise input
python3 denoising_dataset.py --vanilla --channels 512 --kernel-size 11  --no-plot -dir results/adam/vanilla/noise

# adam|vanilla|image input
python3 denoising_dataset.py --vanilla --channels 512 --kernel-size 11  --remove-init --input-image --no-plot -dir results/adam/vanilla/image