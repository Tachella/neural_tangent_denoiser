import torch.nn as nn
import torch
import numpy as np
import torch.nn.functional as F



def init_weights(m):
    if type(m) == nn.Conv2d:
        nn.init.kaiming_normal_(m.weight, nonlinearity='relu')


def VanillaCNN(input_chan, layers=1, channels=64, kernel_size=9):
    model = nn.Sequential()
    need_bias = False
    to_pad = int((kernel_size - 1) / 2)

    l = 0
    model.add_module('ref' + str(l), nn.ReflectionPad2d(to_pad))
    model.add_module('conv' + str(l), nn.Conv2d(input_chan, channels, kernel_size, bias=need_bias))
    model.add_module('relu' + str(l), nn.ReLU())
    l += 1

    for i in range(layers-1):
        model.add_module('ref'+ str(l), nn.ReflectionPad2d(to_pad))
        model.add_module('conv'+ str(l), nn.Conv2d(channels, channels, kernel_size, bias=need_bias))
        model.add_module('relu'+ str(l), nn.ReLU())
        l += 1


    # linear final layer
    kernel_size_out = 1
    to_pad = int((kernel_size_out - 1) / 2)
    model.add_module('ref'+ str(l), nn.ReflectionPad2d(to_pad))
    model.add_module('conv'+ str(l), nn.Conv2d(channels, input_chan, kernel_size_out, bias=need_bias))

    model.apply(init_weights)
    #print(model)
    return model



class ResNet(nn.Module):
    def __init__(self, in_channels, channels, kernel_size):
        super(ResNet, self).__init__()
        self.kernel_size = kernel_size
        to_pad = [int((kernel_size-1)/2)]*2
        padding_mode = 'reflect'
        self.convd1 = nn.Conv2d(in_channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convd2 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convd3 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convd4 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convd5 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convu1 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convu2 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convu3 = nn.Conv2d(channels, channels, kernel_size, bias=False, padding=to_pad, padding_mode=padding_mode)
        self.convout = nn.Conv2d(channels, in_channels, 1, bias=False, padding=0)

        init_weights(self)

    def forward(self, x):
        to_pad = 32
        out = F.pad(x, [to_pad]*4, 'reflect')
        out = self.convd1(out)
        out = F.relu(out)

        ## going down
        out = F.interpolate(out, scale_factor=(0.5, 0.5), mode='bilinear', align_corners=True)
        out = self.convd2(out)
        out = F.relu(out)

        out = F.interpolate(out, scale_factor=(0.5, 0.5), mode='bilinear', align_corners=True)
        out = self.convd3(out)
        out = F.relu(out)

        out = F.interpolate(out, scale_factor=(0.5, 0.5), mode='bilinear', align_corners=True)
        out = self.convd4(out)
        out = F.relu(out)
        out = self.convd5(out)

        # going up
        out = F.interpolate(out, scale_factor=(2, 2), mode='bilinear', align_corners=True)
        out = self.convu1(out)
        out = F.relu(out)

        out = F.interpolate(out, scale_factor=(2, 2), mode='bilinear', align_corners=True)
        out = self.convu2(out)
        out = F.relu(out)

        out = F.interpolate(out, scale_factor=(2, 2), mode='bilinear', align_corners=True)
        out = self.convu3(out)
        out = F.relu(out)

        out = self.convout(out)
        out = out[:, :, to_pad:-to_pad, to_pad:-to_pad]
        return out

