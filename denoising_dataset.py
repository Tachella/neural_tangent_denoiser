#!/usr/bin/env python
# coding: utf-8
# Import libs


from utils.neural_tangent_denoiser import NeuralTangentDenoiser
from utils.common_utils import *
import json
import argparse
import os
import time
import scipy.misc


def save_results(self):
    import json
    dict = {'mse': self.error, 'psnr': self.psnr_iter, 'best_psnr': self.best_psnr}
    json = json.dumps(dict)
    f = open("dict.json", "w")
    f.write(json)
    f.close()


def get_freer_gpu():
    os.system('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free >tmp')
    memory_available = [int(x.split()[2]) for x in open('tmp', 'r').readlines()]
    print('GPU id ' + str(np.argmax(memory_available)) + '|free memory: ' + str(np.max(memory_available)))
    return np.argmax(memory_available)


parser = argparse.ArgumentParser(description='Denoising parameters.')
parser.add_argument('--channels', type=int, default=128, help='number of hidden layer channels')
parser.add_argument('--kernel-size',  type=int, default=3, help='size of the convolution kernel')
parser.add_argument('--repetitions',  type=int, default=1, help='number of denoising repetitions')
parser.add_argument('--show-every',  type=int, default=1000, help='size of the convolution kernel')
parser.add_argument('--max-iter',  type=int, default=1e6, help='size of the convolution kernel')
parser.add_argument('--mc-iter',  type=int, default=1, help='Number of Monte Carlo repetitions')
parser.add_argument('--depth',  type=int, default=1, help='size of the convolution kernel')
parser.add_argument('--sigma',  type=int, default=25, help='standard deviation of the noise \sigma \in [0, 100]')
parser.add_argument('--imsize',  type=int, default=-1, help='size of the image: -1 for original '
                                                           'image size, otherwise size in pixels e.g. 64')
parser.add_argument('--no-plot',  action='store_true', help='do not plot denoising progress')
parser.add_argument('--gray',  action='store_true', help='process grayscale image')
parser.add_argument('--single-image',  action='store_true', help='process only one image')
parser.add_argument('--remove-init',  action='store_true', help='remove initialization from the CNNs output')
parser.add_argument('--input-image',  action='store_true', help='place noisy image at the input'
                                                                ', otherwise place just noise')
parser.add_argument('--vanilla',  action='store_true', help='use vanilla CNN, otherwise uses UNET')
parser.add_argument('--noise2self', action='store_true', help='use noise2self (N2S) training'
                                                               ', please see the N2S paper for details')
parser.add_argument('--gd', action='store_true', help=' use gradient descent, otherwise use Adam')
parser.add_argument('--change-to-gd', action='store_true', help=' starts optimization with adam and then changes to gradient descent')
parser.add_argument('-dir', '--save-dir', default='results/', help='directory to save results')
parser.add_argument('--cpu', action='store_true', help=' use CPU instead of cuda')
parser.add_argument('--nystrom', action='store_true', help=' used closed form kernel with nystrom approximation CPU instead of cuda')
parser.add_argument('--show-preactivations',  action='store_true', help='show the preactivations during'
                                                                        ' training (WARNING: this might be'
                                                                        ' slow for large images)')


params = parser.parse_args()

# params.nystrom = True
# params.input_image = True
# params.remove_init = True
# params.kernel_size = 11
# params.gd = True
# params.vanilla = True

# Load image
if params.single_image:
    dataset_filenames = ['house']
else:
    dataset_filenames = ['house', 'baboon', 'F16', 'lena', 'peppers', 'kodim01', 'kodim02', 'kodim03', 'kodim12']
avg_psnr = 0

free_gpu_id = get_freer_gpu()
torch.cuda.set_device(int(free_gpu_id))

k = 0
for filename in dataset_filenames:
    for r in range(params.mc_iter):
        if r == 0:
            fname = filename
        else:
            fname = filename + str(r)

        if not os.path.exists(params.save_dir + '/denoised_' + fname + '.png'):
            print('Processing ' + str(filename))
            img_np, img_noisy_np = get_image('data/color_dataset/' + filename + '.png', params.imsize, params.gray, params.sigma, d=32)

            start = time.time()
            for rep in range(params.repetitions):
                ntd = NeuralTangentDenoiser(img_noisy_np, img_np, params)
                img_denoised, stats = ntd.denoise()
                if not params.no_plot:
                    plot_image_grid([img_np, img_noisy_np, img_denoised])

                if params.change_to_gd:
                    ntd.change_to_gd()
                    img_denoised, stats = ntd.denoise()

                img_noisy_np = img_denoised.copy()

            stats['elapsed_time'] = time.time() - start
            avg_psnr += stats['best_psnr']
            f = open(params.save_dir + '/stats_' + fname + '.json', "w")
            f.write(json.dumps(stats))
            f.close()
            im = np_to_pil(img_denoised)
            k += 1
            im.save(params.save_dir + '/denoised_' + fname + '.png')

if k > 0:
    print('Average PSNR: ' + str(avg_psnr/k))


