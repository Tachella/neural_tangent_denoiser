#!/usr/bin/env bash

for noise in 5 100
do
    # adam|U-Net|noise input
    python3 denoising_dataset.py --sigma $noise --no-plot -dir results/sigma$noise/adam/unet/noise

    # nystrom|vanilla|image input
    python3 denoising_dataset.py  --sigma $noise --nystrom --vanilla --kernel-size 11  --input-image --no-plot -dir results/sigma$noise/nystrom/vanilla/image

    # adam|U-Net|image input
    python3 denoising_dataset.py  --sigma $noise --remove-init --input-image --no-plot -dir results/sigma$noise/adam/unet/image
done