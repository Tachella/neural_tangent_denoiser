# CNN Denoisers As Non-Local Filters: The Neural Tangent Denoiser

This repository is the official implementation of The Neural Tangent Denoiser.

## Requirements

* [PyTorch](https://pytorch.org/) (1.4.0)
* [NumPy](http://www.numpy.org/) (1.18.1)
* [Matplotlib](https://matplotlib.org/) (3.1.3)
* [Pillow](https://pillow.readthedocs.io/en/latest/index.html) (7.1.2)

To install the latest version of all packages, run
```
pip3 install --user -r requirements.txt
```

This code was tested on Python 3.6.10 on Scientific Linux 7.6 (Nitrogen). We also provide an Anaconda environment.yml file.

## Evaluation
 For a list of optional arguments run
 ```
python3 denoising_dataset.py --h 
```
 
The algorithm uses the deep image prior setting by default (Adam/U-Net/noise input), and processes the [9 color image dataset](http://www.cs.tut.fi/~foi/GCF-BM3D/). 
It is possible to denoise a single image by setting the 

## Main results
We provide a series of bash scripts to run the denoising configurations proposed in the paper.
To evaluate denoising with gradient descent (U-Net and vanilla CNN) run  
```
bash run_gd.sh
```

To evaluate denoising with Adam (U-Net and vanilla CNN) run  
```
bash run_adam.sh
```
To evaluate denoising with  Nystrom's approximation of the analytic filter (vanilla CNN) run  
```
bash run_nystrom.sh
```


To evaluate the change of weights during training with Adam (U-Net/noise input)
```
bash weights_adam.sh
```

To evaluate the change of weights during training with gradient descent (U-Net/noise input)
```
bash weights_gd.sh
```


To evaluate the best performing algorithms in other noise levels 
```
bash noise_levels.sh
```


## Plotting and summary of results
Denoising results can be plotted using 
```
python3 plot_results.sh
```
The script also prints the average PSNR of the different denoising algorithms. You will have to modify the "SETTINGS" of script to print other specific statistics, such as the epoch count.
