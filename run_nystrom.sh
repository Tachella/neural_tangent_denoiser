#!/usr/bin/env bash

# Nystrom|vanilla|image
python3 denoising_dataset.py --nystrom --vanilla --kernel-size 11  --input-image --no-plot -dir results/nystrom/vanilla/image

# Nystrom|vanilla|image
python3 denoising_dataset.py --nystrom --vanilla --kernel-size 11  --no-plot -dir results/nystrom/vanilla/noise
