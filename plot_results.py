import json
import os
from utils.common_utils import get_image, plot_image_grid


#### MAIN SETTINGS #####

single_im = 'house'  # single image to plot ('house' for fig.1)
property = 'best_psnr'  # statistic to compute (e.g. 'best_psnr' or 'epoch')
results_dir = 'results/' # directory where results are saved


## other
architectures = ['unet', 'vanilla']
inputs = ['image', 'noise']
optimizers = ['adam', 'gd', 'nystrom']
dataset_filenames = ['house', 'baboon', 'F16', 'lena', 'peppers', 'kodim01', 'kodim02', 'kodim03', 'kodim12']
im_list = []

for arc in architectures:
    for inp in inputs:
        for opt in optimizers:
            dir = results_dir + opt + '/' + arc + '/' + inp + '/'
            avg = 0
            k = 0
            for images in dataset_filenames:
                dirst = dir + 'stats_' + images + '.json'
                if os.path.exists(dirst):
                    f = open(dirst, "r")
                    stats = json.load(f)
                    if property in stats:
                        avg += stats[property]
                        k += 1
                    f.close()
            if k > 0:
                avg /= k
                print('Optimizer: ' + opt + ' |Architecture: ' + arc + ' |Input: ' + inp + '|Images ' + str(k) + ' |Average ' + property + ': ' + str(avg))

            dirim = dir + 'denoised_' + single_im + '.png'
            if os.path.exists(dirim):
                im, _ = get_image(dirim)
                im_list.append(im)
                dirst = dir + 'stats_' + single_im + '.json'
                f = open(dirst, "r")
                stats = json.load(f)
                print('Optimizer: ' + opt + ' |Architecture: ' + arc + ' |Input: ' + inp + ' |' + single_im + ' PSNR: ' + str(stats[property]))
                f.close()

if im_list:
    plot_image_grid(im_list)
