#!/usr/bin/env bash


for chan in 8 16 32 64 128 256 512
do
    python3 denoising_dataset.py --channels $chan --mc-iter 10 --no-plot --single-image -dir results/adam/unet/noise/ch$chan
done

